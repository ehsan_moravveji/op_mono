
![](logo-small.png "OP Mono")

# OP_Mono Package

## Overview
The purpose of this package is to compute customized Type I opacity tables in the OPAL format
with user-specified Iron and Nickel opacity enhancements (and possible for other elements in 
the future upon request). This package links and interacts with the [MESA](mesa.sourceforge.net)
stellar structure and evolution code. The results of Iron and Nickel enhancements on instability 
domains of massive early-type pulsating stars are published in 
 - [Moravveji E. 2016, MNRAS Letters, 455, Issue 1, L67-71](https://academic.oup.com/mnrasl/article-lookup/doi/10.1093/mnrasl/slv142)
where you can refer to for more details. 

## Download, Instal and Use
The [Wiki](https://bitbucket.org/ehsan_moravveji/op_mono/wiki/Home) page gives you a full 
description of the state of the package, and also walks you through every installation and test 
steps. Even without getting your hands dirty, you can already download three sets of pre-computed
opacity tables that are used in Moravveji (2015) from the links given in the 
[Wiki](https://bitbucket.org/ehsan_moravveji/op_mono/wiki/Home) page. Therefore, that is the 
best start!

## Point of Contact
Please contact Ehsan Moravveji (at KU Leuven until mid-2017) for any inquiries like questions, 
bug report, feature request etc. If you happen to use OP_Mono package, you can contact the 
owner to include you in the users' list in order to receive notifications on possible updates,
upgrades and modifications.

## Acknowledgment
The research leading to these results has received funding from the People Programme 
(Marie Curie Actions) of the European Union’s Seventh Framework Pro- gramme FP7/2007-2013/ 
under REA grant agreement n◦ 623303 for the project ASAMBA. The computational resources and 
services used in this work were provided by the VSC (Flemish Supercomputer Center), funded by 
the Hercules Foundation and the Flemish Government - department EWI.
