!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!     Copyright (C) 2015   Ehsan Moravveji
!
!     This program is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.

!     This program is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.

!     You should have received a copy of the GNU General Public License
!     along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! - Date: Saturday 16 May, 2015
! - Author: Ehsan Moravveji, KU Leuven, Belgium
! - Version: Adapted to MESA v.7385
! - Purpose: Compute OP Mono opacities
! - Name conventions:
!        ln: natural logarithm
!        log: logarithm in base 10 

module benchmark

  use omp_lib
  use convenience_lib

  implicit none 

  contains 

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine check_omp_speedup(inlist_path, dt_sec, ierr)
    character(len=*), intent(in) :: inlist_path
    real(dp), intent(out) :: dt_sec
    integer, intent(out) :: ierr

    integer :: i, num_tries, num_thread, num_max_threads
!     integer, allocatable, dimension(:) :: thread_array
!     real(dp), allocatable, dimension(:) :: runtime_array
    integer :: t_start, t_end, clock_rate
    real(dp), parameter :: X=0d0, Z=0d0
    type(op_mono_table), pointer :: self
    integer, parameter :: load_id = 100
    character(len=256) :: cmnd           ! to set OMP_NUM_THREADS
    character(len=256) :: actual_OMP_NUM_THREADS

    ierr = 0

    num_max_threads = omp_get_max_threads()
!     if (num_max_threads == 1) then 
!        write(*,*) ' - benchmark: check_omp_speedup: There is only one thread available. Returning ...'
!        return
!     endif
    num_tries = num_max_threads / 2 + 1

    print*
    print "(' - benchmark: check_omp_speedup')"
    print "('   omp_get_max_threads = ', i2)", num_max_threads

!     allocate(thread_array(num_tries), runtime_array(num_tries), stat=ierr)
!     if (ierr /= 0) then 
!        write(*,*) 'Error: benchmark: check_omp_speedup: allocate failed'
!        return
!     endif
!     thread_array(1)  = 1
!     do i = 2, num_tries 
!        thread_array(i) = 2 * (i - 1)
!     enddo
!     runtime_array(:) = 0d0

    print*, '   Loading OP Mono data takes few seconds, and is taken out of the loop below here'
    print*, '   OP Mono data needs to be loaded once, thus the overhead is not distributed among'
    print*, '   all threads equally'
    call get_table_ptr(id=load_id, self=self, ierr=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: benchmark: check_omp_speedup: get_table_ptr failed'
       return 
    endif

    call init(load_id, inlist_path, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: benchmark: check_omp_speedup: init failed'
       return
    endif

!     ! Now, loop over increasing number of threads, and get the runtime in seconds
!     do i = 1, num_tries

       call system_clock(t_start, clock_rate)
!        num_thread = thread_array(i)

!        if (num_thread < 10) then 
!           write(cmnd, '(a,i1,a1)') "export 'OMP_NUM_THREADS=", num_thread, "'"
!        else 
!           write(cmnd, '(a,i2,a1)') "export 'OMP_NUM_THREADS=", num_thread, "'"
!        endif
!        print "('   iteration #', i2, ', command: ', a)", i, cmnd
!        call system(cmnd, ierr)
!        if (ierr /= 0) then 
!           write(*,*) 'Error: benchmark: check_omp_speedup: system call failed for num_thread=', num_thread
!           return
!        endif

!        call get_environment_variable('OMP_NUM_THREADS', actual_OMP_NUM_THREADS)
!        print *, i, num_thread, actual_OMP_NUM_THREADS

       call compute_one_set(1, inlist_path, X, Z, ierr)
       if (ierr /= 0) then 
          write(*,*) 'Error: benchmark: check_omp_speedup: compute_one_set failed'
          return
       endif

       call system_clock(t_end, clock_rate)
       dt_sec = dble(t_end - t_start) / clock_rate
!        runtime_array(i) = dt_sec

!     enddo

!     print*
!     print "(2a16)", 'N_thread', 'RunTime [sec]'
!     do i = 1, num_max_threads
!        print "(i16, f16.3)", thread_array(i), runtime_array(i)
!     enddo
!     print*

    contains

      subroutine compute_one_set(id, inlist_path, X, Z, ierr)
        integer, intent(in) :: id 
        character(len=*), intent(in) :: inlist_path
        real(dp), intent(in) :: X, Z
        integer, intent(out) :: ierr

        type(op_mono_table), pointer :: self


        call get_table_ptr(id=id, self=self, ierr=ierr)
        if (ierr /= 0) then 
           write(*,*) 'Error: benchmark: check_omp_speedup: get_table_ptr failed'
           return 
        endif

        call set_table_XZ(id=id, X=X, Z=Z, ierr=ierr)
        if (ierr /= 0) then 
            write(*,*) 'Error: benchmark: check_omp_speedup: set_table_XZ failed'
            return 
        endif
 
        call init(id, inlist_path, ierr)
        if (ierr /= 0) then 
           write(*,*) 'Error: benchmark: check_omp_speedup: init failed'
           return
        endif

        call get_kappa_block(id=id, ierr=ierr)
        if (ierr /= 0) then 
           write(*,*) 'Error: benchmark: check_omp_speedup: get_kappa_block failed'
           return
        endif

      end subroutine compute_one_set


  end subroutine check_omp_speedup

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


end module benchmark
