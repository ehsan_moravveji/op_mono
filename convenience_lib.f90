!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!     Copyright (C) 2015   Ehsan Moravveji
!
!     This program is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.

!     This program is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.

!     You should have received a copy of the GNU General Public License
!     along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!

module convenience_lib

  use op_mono_lib
  use writer

  implicit none

  contains

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! To query the OP online server available at http://opacities.osc.edu/rmos.shtml
  ! to compute Rosseland mean opacities, pick H, He and the metal mass fractions using this

  subroutine gen_op_server_metal_fraction(inlist_path, ierr)
    character(len=*), intent(in) :: inlist_path
    integer, intent(out) :: ierr

    integer :: i, id
    type(op_mono_table), pointer :: self
    real(dp), allocatable, dimension(:) :: normalized_metals

    id = get_table_id()
    call get_table_ptr(id=id, self=self, ierr=ierr)
    if (ierr /= 0) return
    call set_table_XZ(id=id, X=self% base_X, Z=self% base_Z, ierr=ierr)
    if (ierr /= 0) return
    call init(id=id, inlist_full_path=inlist_path, ierr=ierr)
    if (ierr /= 0) return
    call renormalize_Zi_metals_to_unity(id, normalized_metals, ierr)
    if (ierr /= 0) return

    print "(a10, 2a16)", 'Element', 'Original', 'Normalized'
    print "(a10, 2a16)", 'Name', 'mass_frac', 'mass_frac'
    do i = 1, self% num_elem 
       if (i < 3) then 
           print "(a10, F16.6, a16)", self% elem_name(i), self% base_mass_fracs(i), '...'
       else
           print "(a10, 2F16.6)", self% elem_name(i), self% base_mass_fracs(i), normalized_metals(i-2)
       endif
    enddo
    print "('  base_X=', F0.4, ', base_Y=', F0.4, ', base_Z=', F0.4)", &
               self% base_X, self% base_Y, self% base_Z
    print*

    call free_table_ptr(id=id, ierr=ierr)
    if (ierr /= 0) return

  end subroutine gen_op_server_metal_fraction

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine gen_mesa_uniform_xa_file(inlist_path, ierr)
    character(len=*), intent(in) :: inlist_path
    integer, intent(out) :: ierr

    integer :: id
    type(op_mono_table), pointer :: self

    id = get_table_id()
    call get_table_ptr(id=id, self=self, ierr=ierr)
    if (ierr /= 0) return
    call set_table_XZ(id=id, X=self% base_X, Z=self% base_Z, ierr=ierr)
    if (ierr /= 0) return
    call init(id=id, inlist_full_path=inlist_path, ierr=ierr)
    if (ierr /= 0) return
    call list_table_content(id=id, ierr=ierr)
    if (ierr /= 0) return
    call write_mesa_uniform_xa_file(id, '../uniform_xa.txt', ierr)
    if (ierr /= 0) return
    call free_table_ptr(id=id, ierr=ierr)
    if (ierr /= 0) return

  end subroutine gen_mesa_uniform_xa_file

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine gen_and_write_one_Type_II_table(inlist_path, X, Z, ierr)
    character(len=*), intent(in) :: inlist_path
    real(dp), intent(in) :: X, Z
    integer, intent(out) :: ierr

  end subroutine gen_and_write_one_Type_II_table

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine gen_Type_I_tables(inlist_path, ierr)
    character(len=*), intent(in) :: inlist_path
    integer, intent(out) :: ierr

    integer :: id 
    integer, dimension(max_num_tables) :: id_array     ! (max_num_tables)
    type(op_mono_table), pointer :: self
    integer :: t_start, t_end, clock_rate
    real(dp) :: dt_sec, dt_min

    call system_clock(t_start, clock_rate)

    call report_host_spec(ierr)

    call compute_set_kappa_Type_I(inlist_path, id_array, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: convenience_lib: gen_Type_I_tables: compute_set_kappa_Type_I failed'
       return 
    endif

    call write_op_mono_type_I_tables(id_array, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: convenience_lib: gen_Type_I_tables: write_op_mono_type_I_tables failed'
       return
    endif

    do id = 1, max_num_tables
       call free_table_ptr(id, ierr)
       if (ierr /= 0) then 
          write(*,'(a, i4)') 'Error: convenience_lib: gen_Type_I_tables: free_table_ptr failed for id: ', id
          return
       endif
    enddo

    call report_host_spec(ierr)

    call system_clock(t_end, clock_rate)
    dt_sec = dble(t_end - t_start) / clock_rate 
    dt_min = dt_sec / 60
    print*
    print "(' convenience_lib: gen_Type_I_tables: Runtime:')"
    print "(' dT = ', f0.4, ' [sec]')", dt_sec
    print "(' dT = ', f0.4, ' [min]')", dt_min
    print*

  end subroutine gen_Type_I_tables

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine gen_and_write_one_Type_I_table(inlist_path, X, Z, ierr)
    character(len=*), intent(in) :: inlist_path
    real(dp), intent(in) :: X, Z
    integer, intent(out) :: ierr

    integer :: id 
    type(op_mono_table), pointer :: self
    integer :: t_start, t_end, clock_rate
    real(dp) :: dt_sec, dt_min

    call system_clock(t_start, clock_rate)

    call report_host_spec(ierr)

    id = get_table_id()
    call get_table_ptr(id=id, self=self, ierr=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: convenience_lib: gen_and_write_one_Type_I_table: get_table_ptr failed'
       return 
    endif

    call set_table_XZ(id=id, X=X, Z=Z, ierr=ierr)
    if (ierr /= 0) then 
        write(*,*) 'Error: convenience_lib: gen_and_write_one_Type_I_table: set_table_XZ failed'
        return 
    endif

    call init(id, inlist_path, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: convenience_lib: gen_and_write_one_Type_I_table: init failed'
       return
    endif

    call get_kappa_block(id=id, ierr=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: convenience_lib: gen_and_write_one_Type_I_table: get_kappa_block failed'
       return
    endif

    call write_op_mono_type_I_table(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: convenience_lib: gen_and_write_one_Type_I_table: write_op_mono_type_I_table failed'
       return
    endif

    call free_table_ptr(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: convenience_lib: gen_and_write_one_Type_I_table: free_table_ptr failed'
       return
    endif

    call report_host_spec(ierr)

    call system_clock(t_end, clock_rate)
    dt_sec = dble(t_end - t_start) / clock_rate 
    dt_min = dt_sec / 60
    print*
    print "(' convenience_lib: gen_and_write_one_Type_I_table: Runtime:')"
    print "(' dT = ', f0.4, ' [sec]')", dt_sec
    print "(' dT = ', f0.4, ' [min]')", dt_min
    print*

  end subroutine gen_and_write_one_Type_I_table

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

end module convenience_lib
