!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!     Copyright (C) 2015   Ehsan Moravveji
!
!     This program is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.

!     This program is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.

!     You should have received a copy of the GNU General Public License
!     along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!

module io_tools

  implicit none

  public 

  integer, parameter :: max_iounit = 10
  integer :: i
  integer, parameter, dimension(max_iounit) :: list_iounit = [ (i, i=1, max_iounit) ]
  logical, dimension(max_iounit) :: iounit_status = .false.

  contains 

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine report_host_spec(ierr)
    integer, intent(out) :: ierr
    character(len=256) :: OMP_NUM_THREADS, date, hostname
    integer, dimension(8) :: date_time_vals
    character(len=256) :: date_time_str

    ierr = hostnm(hostname)
    if (ierr /= 0) then 
       write(*,*) 'Error: io_tools: report_host_spec: hostnm failed'
       return
    endif
 
    call date_and_time(values=date_time_vals)

    write(date_time_str, "(2(i2,x), i4, 2x, 3(i2,':'))") &
          date_time_vals(3), date_time_vals(2), date_time_vals(1), &
          date_time_vals(5), date_time_vals(6), date_time_vals(7)

    call get_environment_variable('OMP_NUM_THREADS', OMP_NUM_THREADS, trim_name=.true.)

    print*
    print "('  hostname: ')", trim(hostname)
    print "('  date: ', a)", trim(date_time_str)
    print "('  OMP_NUM_THREADS = ', a)", OMP_NUM_THREADS
    print*


  end subroutine report_host_spec

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! The path to some files needs to be prepended by the path to the user's external directory
  ! to return a full path. The user external directory should be alreay set in the .bash_profile
  ! as USER_DIR. This environment variable is retrieved to prepend the relative filename; thus,
  ! it should be already set.

  subroutine prepend_USER_DIR_to_path(rel_path, full_path, ierr) 
    character(len=256), intent(in) :: rel_path
    character(len=256), intent(out) :: full_path
    integer, intent(out) :: ierr 

    character(len=256) :: USER_DIR

    call get_environment_variable('USER_DIR', USER_DIR)
    if (len_trim(USER_DIR) == 0) then 
       write(*,*) 'Error: io_tools: prepend_USER_DIR_to_path: USER_DIR is not set in ~/.bash_profile'
       ierr = -1
       return
    endif

    call check_dir(USER_DIR, .false., ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: io_tools: prepend_USER_DIR_to_path: check_dir failed'
       return
    endif
    
    call check_dir_trailing_slash(USER_DIR, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: io_tools: prepend_USER_DIR_to_path: check_dir_trailing_slash failed'
       return
    endif
    
    full_path = trim(USER_DIR) // trim(rel_path)

  end subroutine prepend_USER_DIR_to_path

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! The path to local inlist files needs to be prepended by the path to the current working 
  ! directory, i.e. cwd, to return a full path

  subroutine prepend_cwd_to_path(rel_path, full_path, ierr) 
    character(len=256), intent(in) :: rel_path
    character(len=256), intent(out) :: full_path
    integer, intent(out) :: ierr 

    character(len=256) :: cwd

    call getcwd(cwd, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: io_tools: prepend_cwd_to_path: getcwd failed'
       return
    endif

    call check_dir(cwd, .false., ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: io_tools: prepend_cwd_to_path: check_dir failed'
       return
    endif
    
    call check_dir_trailing_slash(cwd, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: io_tools: prepend_cwd_to_path: check_dir_trailing_slash failed'
       return
    endif
    
    full_path = trim(cwd) // trim(rel_path)

  end subroutine prepend_cwd_to_path

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! If a directory does not exist, it will be created by a system call
  ! Note that the inquire command works differently with ifort; in such case, it should be 
  ! inquire(directory=trim(dir), exist=dir_exists, iostat=ierr)

  subroutine check_dir(dir, mkdir, ierr)
    character(len=*), intent(in) :: dir
    integer, intent(out) :: ierr
    logical, intent(in) :: mkdir

    logical :: dir_exists
    character(len=256) :: cmnd

    inquire(file=trim(dir) // '/.', exist=dir_exists, iostat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: io_tools: check_dir: inquire failed'
       return
    endif

    if (.not. dir_exists) then 
      if (mkdir) then 
         cmnd = 'mkdir -p ' // trim(dir)
         call system(cmnd)
         print "(' io_tools: check_dir: Created ', a)", trim(dir)
         ierr = 0
      endif
    endif

  end subroutine check_dir

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! Append a trailing '/' to the directory name if it already does not have it

   subroutine check_dir_trailing_slash(dir, ierr)
     character(len=*)     :: dir 
     integer, intent(out) :: ierr

     integer :: n
     integer :: k

     n = len_trim(dir)
     k = index(dir, '/', back=.true.)
     if (k == 0) then 
       write(*,*) 'Error: io_tools: check_dir_trailing_slash: input directory name has zero length.'
       ierr = -1
       return 
     endif
     if(k /= n) then
       write(dir(n+1:),"('/')")
     endif

   end subroutine check_dir_trailing_slash  

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine inquire_file_exists(file_path, ierr)
    character(len=*), intent(in) :: file_path
    integer, intent(out) :: ierr
    logical :: file_exists 

    ierr = 0
    inquire(file=trim(file_path), exist=file_exists, iostat=ierr)
    if (.not. file_exists .or. ierr /= 0) then
       write(*,*) 'Error: io_tools: inquire_file_exists: file not found; ierr = ', ierr
       return
    endif

  end subroutine inquire_file_exists

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  function get_iounit() result(unit)
    integer :: unit   ! acts both as the IO unit and error handler

    integer :: i

    unit = -1         ! initialize to erratic value
    do i = 1, max_iounit
       if (.not. iounit_status(i)) then 
          unit = i 
          iounit_status(i) = .true.
          exit
       endif
    enddo
    if (unit == -1) then 
       write(*,*) 'Error: io_tools: get_iounit: Exceeding allowed open IO units'
       unit = -1
       return
    endif

  end function get_iounit

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine free_iounit(unit, ierr)
    integer, intent(in)  :: unit
    integer, intent(out) :: ierr

    ierr = 0
    if (unit < 1 .or. unit > max_iounit) then 
       write(*,*) 'Error: io_tools: free_iounit: requested unit outside allowed range of IO units'
       ierr = -1
       return
    endif
    if (iounit_status(unit)) iounit_status(unit) = .false.

  end subroutine free_iounit

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

end module io_tools

