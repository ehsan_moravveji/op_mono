!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!     Copyright (C) 2015   Ehsan Moravveji
!
!     This program is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.

!     This program is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.

!     You should have received a copy of the GNU General Public License
!     along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! - Date: Friday 22 May, 2015
! - Author: Ehsan Moravveji, KU Leuven, Belgium
! - Version: Adapted to MESA v.7385
! - Purpose: Tabulate OP Mono opacities
! - Name conventions:
!

module op_mono_io

  use const_def
!  use op_mono_lib

  implicit none

  public

  integer, parameter :: max_iounit = 10
  integer :: i
  integer, parameter, dimension(max_iounit) :: list_iounit = [ (i, i=1, max_iounit) ]
  logical, dimension(max_iounit) :: iounit_status = .false.

  type, public :: user_settings
    integer  :: mixture = 0
    real(dp) :: Fe_kappa_factor
    real(dp) :: Ni_kappa_factor
  end type user_settings

  contains

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine read_inlist_op_mono(inlist_path, set, ierr)
    character(len=*), intent(in) :: inlist_path
    type (user_settings), intent(out) :: set
    integer, intent(out) :: ierr

    namelist / op_mono_nmlst / &
             mixture, &
             Fe_kappa_factor, Ni_kappa_factor

    integer :: mixture
    real(dp) :: Fe_kappa_factor, Ni_kappa_factor

    integer :: iounit

    ierr = 0
    call inquire_file_exists(inlist_path, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_io: read_inlist_op_mono: inquire_file_exists failed, ierr=', ierr
       return
    endif

    iounit = get_iounit()
    if (iounit == -1) then 
       write(*,*) 'Error: op_mono_io: read_inlist_op_mono: get_iounit failed'
       ierr = iounit
       return
    endif

    open(unit=iounit, file=trim(inlist_path), status='old', action='read', iostat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_io: read_inlist_op_mono: open stetement ailed'
       return
    endif

    read(unit=iounit, nml=op_mono_nmlst, iostat=ierr)

    set% mixture = mixture
    set% Fe_kappa_factor = Fe_kappa_factor
    set% Ni_kappa_factor = Ni_kappa_factor

    close(unit=iounit)
    
    call free_iounit(iounit, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_io: read_inlist_op_mono: free_iounit failed'
       return 
    endif

  end subroutine read_inlist_op_mono
  
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine inquire_file_exists(file_path, ierr)
    character(len=*), intent(in) :: file_path
    integer, intent(out) :: ierr
    logical :: file_exists 

    ierr = 0
    inquire(file=trim(file_path), exist=file_exists, iostat=ierr)
    if (.not. file_exists .or. ierr /= 0) then
       write(*,*) 'Error: op_mono_io: inquire_file_exists: file not found; ierr = ', ierr
       return
    endif

  end subroutine inquire_file_exists

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  function get_iounit() result(unit)
    integer :: unit   ! acts both as the IO unit and error handler

    integer :: i

    unit = -1         ! initialize to erratic value
    do i = 1, max_iounit
       if (.not. iounit_status(i)) then 
          unit = i 
          iounit_status(i) = .true.
          exit
       endif
    enddo
    if (unit == -1) then 
       write(*,*) 'Error: op_mono_io: get_iounit: Exceeding allowed open IO units'
       unit = -1
       return
    endif

  end function get_iounit

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine free_iounit(unit, ierr)
    integer, intent(in)  :: unit
    integer, intent(out) :: ierr

    ierr = 0
    if (unit < 1 .or. unit > max_iounit) then 
       write(*,*) 'Error: op_mono_io: free_iounit: requested unit outside allowed range of IO units'
       ierr = -1
       return
    endif
    if (iounit_status(unit)) iounit_status(unit) = .false.

  end subroutine free_iounit

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

end module op_mono_io
