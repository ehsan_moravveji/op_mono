!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!     Copyright (C) 2015   Ehsan Moravveji
!
!     This program is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.

!     This program is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.

!     You should have received a copy of the GNU General Public License
!     along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! - Date: Saturday 16 May, 2015
! - Author: Ehsan Moravveji, KU Leuven, Belgium
! - Version: Adapted to MESA v.7385
! - Purpose: Compute OP Mono opacities
! - Name conventions:
!        ln: natural logarithm
!        log: logarithm in base 10 
!
 
module op_mono_lib 

  use const_def
  use chem_def
  use chem_lib
  use kap_def
  use kap_lib
  use eos_def
  use eos_lib

  use reader
  
  implicit none 
    
!  private
!  public :: get_op_mono_kappa

  type, public, extends(user_settings) :: abund  ! user_settings is defined in reader
   integer, allocatable, dimension(:)  :: chem_id

   ! The base and the working XYZ
   real(dp) :: base_X 
   real(dp) :: base_Y
   real(dp) :: base_Z
   real(dp) :: X = -1d0     ! initial value is checked by set_table_XZ()
   real(dp) :: Z = -1d0     ! initial value is checked by set_table_XZ()
 
   real(dp), allocatable, dimension(:) :: original_Z_fracs ! (num_chem_elements)

   integer  :: num_elem
   integer, allocatable, dimension(:)  :: elem_Z              ! (num_elem) = (17)
   character(len=2), allocatable, dimension(:) :: elem_name   ! (num_elem) = (17)
   real(dp), allocatable, dimension(:) :: elem_mass           ! (num_elem) = (17)
   real(dp), allocatable, dimension(:) :: base_mass_fracs     ! (num_elem) = (17) initial values
   real(dp), allocatable, dimension(:) :: mass_fracs          ! (num_elem) = (17) working values

   integer, allocatable, dimension(:)  :: ind_Z_elem          ! (num_elem) = (17)
   real(dp)                            :: abar, zbar

  end type abund


  type, public, extends(abund) :: workings

   integer, allocatable, dimension(:), private  :: izzp
   real(dp), allocatable, dimension(:), private :: fap
   integer, private :: nptot, ipe, nrad, nel 
!    real, pointer, private :: umesh(:), ff(:,:,:,:), rs(:,:,:), s(:,:,:,:)
   real(dp), allocatable, dimension(:) :: logT_array, logR_array

   real(dp), allocatable, dimension(:) :: kappa_factor     ! (num_elem)

  end type workings


  type, public, extends(workings) :: op_mono_table

   integer :: id          ! a unique table ID number
   real(dp), allocatable, dimension(:,:) :: block_log_kappa, &        ! (n_logT, n_logR)
                                            block_dln_kappa_dlnT, &   ! (n_logT, n_logR)
                                            block_dln_kappa_dlnRho    ! (n_logT, n_logR)
   logical :: kap_is_computed = .false.                               ! Initialized to False                                            
  end type op_mono_table

!   type (op_mono_table), target, save :: op_mono

!  interface op_mono_table
!       module procedure init
!  end interface op_mono_table

  integer, parameter :: num_X_Type_I = 10, num_Z_Type_I = 16
  integer, parameter :: num_id_Type_I = 126
  integer, parameter :: max_num_tables = num_id_Type_I
  logical, dimension(max_num_tables) :: table_in_use = .false.
  type(op_mono_table), target, save :: tables(max_num_tables)

  real(dp), parameter :: bad_kappa_val = -99d0 
  integer :: kap_handle, eos_handle
  logical :: kap_handle_is_available = .false.
  logical :: eos_handle_is_available = .false.


  contains


  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! get an appropriate id for a table which is free and not allocated yet
  ! The table_in_use array is set to .true. for the available table in another routine: get_table_ptr()

  integer function get_table_id() result(id)
    integer :: i

    id = 0
    do i = 1, max_num_tables
       if (table_in_use(i)) then 
          cycle
       else 
          id = i
          exit
       endif
    enddo
    if (id == 0) stop 'Error: op_mono_lib: get_table_id: No table is available'

  end function get_table_id

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! get an instance of the op_mono_table derived type to work with

  subroutine get_table_ptr(id, self, ierr)
    integer, intent(in) :: id 
    type(op_mono_table), pointer :: self
    integer, intent(out) :: ierr

    ierr = 0
    if (id < 1 .or. id > max_num_tables) then 
       write(*,'(a, i4)') 'Error: op_mono_lib: get_table_ptr: Invalid id number: id = ', id
       ierr = -1
       return
    endif
    self => tables(id)
    table_in_use(id) = .true.

  end subroutine get_table_ptr

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! To free up the id for the table that was already in use. It is recommended to free the table 
  ! pointer after use, to make sure table id's are accurately handled.

  subroutine free_table_ptr(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self

    ierr = 0
    if (id < 1 .or. id > max_num_tables) then 
       write(*,'(a, i4)') 'Error: op_mono_lib: free_table_ptr: Invalid id number: id = ', id
       ierr = -1
       return
    endif
    if (.not. table_in_use(id)) then 
       write(*,'(a, i4, a)') 'Error: op_mono_lib: free_table_ptr: id = ', id, ' already not in use!'
       ierr = -1
       return
    endif

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: free_table_ptr: get_table_ptr failed'
       return
    endif
    table_in_use(id) = .false.

    if (kap_handle_is_available) call free_kap_handle(kap_handle)
    if (eos_handle_is_available) call free_eos_handle(eos_handle)

  end subroutine free_table_ptr

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine init(id, inlist_full_path, ierr)
    integer, intent(in) :: id       ! Table id
    character(len=*), intent(in) :: inlist_full_path
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    type(user_settings) :: set      
    character(len=256), parameter :: default_inlist_rel_path = 'op_mono/inlist_op_mono.defaults'
    character(len=256) :: default_inlist_full_path 

    integer :: mixture

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init: get_table_ptr failed'
       return
    endif

    ! read and store default control values from the local default inlist file
    call prepend_USER_DIR_to_path(default_inlist_rel_path, default_inlist_full_path, ierr) 
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init: prepend_USER_DIR_to_path failed'
       return 
    endif

    call read_inlist_op_mono(default_inlist_full_path, set, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init: read_inlist_op_mono failed to read default inlist!'
       return
    endif

    call update_lib_with_io(id, set, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init: update_lib_with_io failed to update default inlist values!'
       return
    endif

    ! read and store the user-supplied inlist
    call read_inlist_op_mono(inlist_full_path, set, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init: read_inlist_op_mono failed'
       return
    endif

    call update_lib_with_io(id, set, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init: update_lib_with_io failed'
       return
    endif

    ! Allocate and store arrays of information about abundances under the hood
    call init_mixture(id, ierr)
    if (ierr /= 0) then 
      write(*,*) 'Error: op_mono_lib: init: init_mixture failed'
      return
    endif

    call init_eos(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init: init_eos failed'
       return
    endif

    ! Initiate the kap_lib and kap_def public modules
    if (.not. kap_is_initialized) then 
      call init_kappa(id, ierr)
      if (ierr /= 0) then 
         write(*,*) 'Error: op_mono_lib: init: init_kappa failed'
         return
      endif
    endif

    call prepare_kappa(id, ierr)
    if (ierr /= 0) then
       write(*,*) 'Error: op_mono_lib: init: prepare_kappa failed'
       return
    endif

  end subroutine init

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine compute_set_kappa_Type_I(inlist_full_path, id_array, ierr)
    character(len=*), intent(in) :: inlist_full_path
    integer, dimension(num_id_Type_I), intent(out) :: id_array
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: i_tbl, id
    real(dp) :: X, Z
    real(dp), dimension(2, num_id_Type_I) :: XZ_pairs

    XZ_pairs(1:2, 1:num_id_Type_I) = get_XZ_pairs_Type_I()

    do i_tbl = 1, num_id_Type_I
       id = i_tbl
       id_array(i_tbl) = id

       call get_table_ptr(id, self, ierr)
       if (ierr /= 0) then 
          write(*,*) 'Error: op_mono_lib: compute_set_kappa_Type_I: get_table_ptr failed for id=', id
          exit
       endif

       X       = XZ_pairs(1, i_tbl)
       Z       = XZ_pairs(2, i_tbl)
       call set_table_XZ(id, X, Z, ierr)
       if (ierr /= 0) then 
         write(*,*) 'Error: op_mono_lib: compute_set_kappa_Type_I: set_table_XZ failed'
         return
       endif

       call init(id, inlist_full_path, ierr)
       if (ierr /= 0) then 
          write(*,*) 'Error: op_mono_lib: compute_set_kappa_Type_I: init failed for id=', id
          exit
       endif

       call get_kappa_block(id, ierr)
       if (ierr /= 0) then 
          write(*,*) 'Error: op_mono_lib: compute_set_kappa_Type_I: get_kappa_block failed'
          return 
       endif

    enddo
    if (ierr /= 0) return

  end subroutine compute_set_kappa_Type_I

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine get_kappa_block(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: status
    real(dp), dimension(:), allocatable :: logT_array, logR_array
!    real(dp), parameter :: tolerance = epsilon(real(0.0, kind=dp))
    integer :: i_T, i_R, n_T, n_R
    real(dp) :: logR, logT, logRho, log_kappa, dln_kap_dlnT, dln_kap_dlnRho

    logT_array = get_logT_array()
    logR_array = get_logR_array()
    n_T        = size(logT_array)
    n_R        = size(logR_array)

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_kappa_block: get_table_ptr failed for id=', id
       return
    endif

    if (self% X < 0 .or. self% Z < 0) then
       write(*,*) 'Error: op_mono_lib: get_kappa_block: self% X and/or self% Z are not properly set yet!'
       write(*,*) 'X, Z = ', self% X, self% Z 
       ierr = -1
       return
    endif

    allocate(self% logT_array(n_T), self% logR_array(n_R), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_kappa_block: allocate logT_array, logR_array failed'
       return
    endif
    self% logT_array(:) = logT_array(:)
    self% logR_array(:) = logR_array(:)

    if (allocated(self% block_log_kappa)) then 
      deallocate(self% block_log_kappa, self% block_dln_kappa_dlnT, self% block_dln_kappa_dlnRho, stat=ierr)
      if (ierr /= 0) then 
         write(*,*) 'Error: op_mono_lib: get_kappa_block: failed to deallocate block matrixes'
         return
      endif
    endif
    allocate(self% block_log_kappa(n_T, n_R), self% block_dln_kappa_dlnT(n_T, n_R), &
             self% block_dln_kappa_dlnRho(n_T, n_R), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_kappa_block: allocate block matrixes failed'
       return
    endif

    !$OMP PARALLEL DO DEFAULT(NONE) PRIVATE(i_R, i_T, logR, logT, logRho, log_kappa, dln_kap_dlnT, dln_kap_dlnRho, status) &
    !$OMP SHARED(n_R, n_T, logR_array, logT_array, id, self) COLLAPSE(2)
    loop_R: do i_R = 1, n_R 

               loop_T: do i_T = 1, n_T
                          logR = logR_array(i_R)
                          logT = logT_array(i_T)
                          logRho = get_logRho(logT, logR)

                          call get_kappa(id, logT, logRho, &
                                         log_kappa, dln_kap_dlnT, dln_kap_dlnRho, &
                                         status)
                          if (status == 0) then
                             self% block_log_kappa(i_T, i_R)        = log_kappa
                             self% block_dln_kappa_dlnT(i_T, i_R)   = dln_kap_dlnT
                             self% block_dln_kappa_dlnRho(i_T, i_R) = dln_kap_dlnRho
                          else 

                             call get_alternate_kappa(id, logT, logRho, &
                                         log_kappa, dln_kap_dlnT, dln_kap_dlnRho, &
                                         status)
                             if (status /= 0) then 
                                 self% block_log_kappa(i_T, i_R)        = bad_kappa_val
                                 self% block_dln_kappa_dlnT(i_T, i_R)   = bad_kappa_val
                                 self% block_dln_kappa_dlnRho(i_T, i_R) = bad_kappa_val
                             endif
                             self% block_log_kappa(i_T, i_R)        = log_kappa
                             self% block_dln_kappa_dlnT(i_T, i_R)   = dln_kap_dlnT
                             self% block_dln_kappa_dlnRho(i_T, i_R) = dln_kap_dlnRho

                          endif

               enddo loop_T

    enddo loop_R
    !$OMP END PARALLEL DO 

    self% kap_is_computed = .true.

    call list_table_content(id, ierr)
    if (ierr /= 0) return

  end subroutine get_kappa_block

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine get_kappa(id, logT, logRho, &
                       log_kappa, d_ln_kappa_d_lnT, d_ln_kappa_d_lnRho, ierr)
    integer, intent(in) :: id
    real(dp), intent(in) :: logT, logRho
    real(dp), intent(out) :: log_kappa, d_ln_kappa_d_lnT, d_ln_kappa_d_lnRho
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer, allocatable, dimension(:) :: chem_id
    real(dp), dimension(:), allocatable :: mass_fracs
    real(dp), parameter :: min_X_to_include = 1d-12

    integer :: num_elem
    integer, allocatable, dimension(:)  :: izzp
    real(dp), allocatable, dimension(:) :: fap, throw_away
    logical, parameter :: screening = .true.
    real(dp), allocatable, dimension(:) :: coeff   
    integer :: nptot, ipe, nrad, nel     
    real, pointer :: umesh(:), ff(:,:,:,:), rs(:,:,:), s(:,:,:,:)

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib get_kappa: get_table_ptr failed for id=', id
       return
    endif

    num_elem   = self% num_elem
    allocate(izzp(num_elem), fap(num_elem), throw_away(num_elem), coeff(num_elem), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_kappa: allocate failed'
       return
    endif
    nptot             = self% nptot
    ipe               = self% ipe 
    nrad              = self% nrad
    nel               = self% nel
    izzp(1:num_elem)  = self% izzp(1:num_elem)
    fap(1:num_elem)   = self% fap(1:num_elem)
    coeff(1:num_elem) = self% kappa_factor(1:num_elem)

    allocate(umesh(nptot), ff(nptot, ipe, 4, 4), &
             rs(nptot, 4, 4), s(nptot, nrad, 4, 4), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_kappa: allocate umesh, ff, rs, s failed'
       return
    endif

    call op_mono_get_kap( &
           nel, izzp, fap, coeff, logT, logRho, screening, &
           log_kappa, d_ln_kappa_d_lnT, d_ln_kappa_d_lnRho, throw_away, &
           umesh, ff, rs, s, ierr)
    if (ierr /= 0) then 
!        write(*,*) 'Error: op_mono_lib: get_kappa: op_mono_get_kap failed; ierr=', ierr
       if (ierr == 101) write(*,*) '   rho out of range for this T'
       if (ierr == 102) write(*,*) '   T out of range'
       return
    endif

    deallocate(umesh, ff, rs, s, stat=ierr)

  end subroutine get_kappa

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


  subroutine get_alternate_kappa(id, logT, logRho, &
                       log_kappa, d_ln_kappa_d_lnT, d_ln_kappa_d_lnRho, ierr)
    use crlibm_lib
    integer, intent(in) :: id
    real(dp), intent(in) :: logT, logRho
    real(dp), intent(out) :: log_kappa, d_ln_kappa_d_lnT, d_ln_kappa_d_lnRho
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
!     integer :: kap_handle
    real(dp) :: zbar
    real(dp) :: lnfree_e, d_lnfree_e_dlnRho, d_lnfree_e_dlnT
    real(dp) :: X, Z, kap

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_alternate_kappa: get_table_ptr failed'
       return
    endif
    zbar = self% zbar

    call get_eos_lnfree_e(id, logT, logRho, lnfree_e, d_lnfree_e_dlnRho, d_lnfree_e_dlnT, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_alternate_kappa: get_eos_lnfree_e failed'
       return
    endif

    call kap_get_Type1(kap_handle, zbar, X, Z, logRho, logT, &
                       lnfree_e, d_lnfree_e_dlnRho, d_lnfree_e_dlnT, &
                       kap, d_ln_kappa_d_lnRho, d_ln_kappa_d_lnT, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_alternate_kappa: kap_get_Type1 failed'
       return
    endif
    log_kappa = safe_log10_cr(kap)

!     call free_kap_handle(kap_handle)

  end subroutine get_alternate_kappa

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine init_eos(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    real(dp) :: abar, zbar
    integer  :: num_elem
    character(len=*), parameter :: eos_file_prefix = 'mesa', &
                                   eosDT_cache_dir = '', &
                                   eosPT_cache_dir = '', &
                                   eosDE_cache_dir = ''
    logical, parameter :: use_cache = .false.

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_eos: get_table_ptr failed'
       return
    endif
    num_elem = self% num_elem
    abar     = self% abar
    zbar     = self% zbar 

    call eos_init(eos_file_prefix, eosDT_cache_dir, eosPT_cache_dir, eosDE_cache_dir, &
            use_cache, ierr)      
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_eos: call to MESA eos_init failed'
       return
    endif

    if (.not. eos_handle_is_available) then 
      eos_handle = alloc_eos_handle(ierr)
      if (ierr /= 0) then 
         write(*,*) 'Error: op_mono_lib: init_eos: alloc_eos_handle failed'
         return
      endif
      eos_handle_is_available = .true.
    endif

  end subroutine init_eos

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine get_eos_lnfree_e(id, logT, logRho, lnfree_e, d_lnfree_e_dlnRho, d_lnfree_e_dlnT, ierr)
    use chem_def, only: num_chem_isos
    use const_def, only: arg_not_provided
    use eos_def, only: 
    integer, intent(in) :: id 
    real(dp), intent(in) :: logT, logRho
    real(dp), intent(out) :: lnfree_e, d_lnfree_e_dlnRho, d_lnfree_e_dlnT
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: j, num_elem
    real(dp) :: X, Z, abar, zbar
    integer, dimension(:), pointer :: chem_id, net_iso
    real(dp), dimension(:), allocatable :: Xi

    real(dp) :: res(num_eos_basic_results)                
    real(dp) :: d_dlnRho_const_T(num_eos_basic_results)   
    real(dp) :: d_dlnT_const_Rho(num_eos_basic_results)   
    real(dp) :: d_dabar_const_TRho(num_eos_basic_results) 
    real(dp) :: d_dzbar_const_TRho(num_eos_basic_results) 

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_eos_lnfree_e: get_table_ptr failed'
       return
    endif
    num_elem   = self% num_elem
    X          = self% X 
    Z          = self% Z
    abar       = self% abar
    zbar       = self% zbar

    allocate(chem_id(num_elem), net_iso(num_chem_isos), Xi(num_elem), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_eos_lnfree_e: allocate failed'
       return
    endif
    chem_id(1:num_elem) = self% chem_id(1:num_elem)
    ! Read more info in eos/public/eos_lib, around line 1000:
    ! value is 0 if the iso is not in the current net
    ! else is value between 1 and number of species in current net
    net_iso(1:num_chem_isos) = 0
    do j = 1, num_elem
       net_iso( chem_id(j) ) = j
    enddo
    Xi(1:num_elem) = self% mass_fracs(1:num_elem)

    call eosDT_get(eos_handle, Z, X, abar, zbar,  &
               num_elem, chem_id, net_iso, Xi, &
               arg_not_provided, logRho, arg_not_provided, logT,  &
               res, d_dlnRho_const_T, d_dlnT_const_Rho, &
               d_dabar_const_TRho, d_dzbar_const_TRho, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_eos_lnfree_e: eosDT_get failed'
       return
    endif 

    lnfree_e          = res(i_lnfree_e)
    d_lnfree_e_dlnRho = d_dlnRho_const_T(i_lnfree_e)
    d_lnfree_e_dlnT   = d_dlnT_const_Rho(i_lnfree_e)

  end subroutine get_eos_lnfree_e

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine init_mixture(id, ierr)
    use crlibm_lib, only: crlibm_init
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: i, ind, num_elem, mixture

    real(dp) :: base_X, base_Y, base_Z
    integer, dimension(:), allocatable :: op_Z_ind_array
    character(len=*), parameter :: isotope_file = 'isotopes.data'

    ierr = 0

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_mixture: get_table_ptr failed for id=', id
       return
    endif

    mixture        = self% mixture
    num_elem       = num_op_mono_elements
    self% num_elem = num_op_mono_elements   ! = 17

    ! initialize chem to be able to use chem_def objects and variables
    call crlibm_init
    call chem_init(trim(isotope_file), ierr)
    if (ierr /= 0) then 
        write(*,*) 'Error: op_mono_lib: init_mixture: chem_init failed'
        return
    endif

    if (.not. chem_has_been_initialized) then 
       write(*,*) 'Error: op_mono_lib: init_mixture: chem_has_been_initialized == False'
       ierr = -1
       return
    endif

    call set_op_mono_chem_id(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_mixture: set_op_mono_chem_id failed'
       return
    endif

    call set_op_mono_elem_indices(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_mixture: set_op_mono_elem_indices failed'
       return
    endif

    call init_original_Z_fracs(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_mixture: init_original_Z_fracs failed'
       return
    endif

    call set_original_Zi(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_mixture: set_original_Zi failed'
       return
    endif

    call renormalize_Zi_to_base_Z(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_mixture: renormalize_Zi_to_base_Z failed'
       return
    endif

    call modify_XYZ(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_mixture: modify_XYZ failed'
       return
    endif

    call get_abar_zbar(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_mixture: get_abar_zbar failed'
       return
    endif

    print*, ' init_mixture succeeded.'

  end subroutine init_mixture

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! pass the indices for the metals defined in chem_def to easily get their abundances for the 
  ! OP Mono elements defined in kap_def

  subroutine set_op_mono_elem_indices(id, ierr) 
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: num_elem

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: set_op_mono_elem_indices: get_table_ptr failed for id=', id
       return
    endif

    num_elem = self% num_elem

    allocate(self% ind_Z_elem(num_elem), stat=ierr)
    if (ierr /= 0) then 
      write(*,*) 'Error: op_mono_lib: set_op_mono_elem_indices: allocate failed'
      return
    endif

    self% ind_Z_elem(1)  = e_h
    self% ind_Z_elem(2)  = e_he
    self% ind_Z_elem(3)  = e_c 
    self% ind_Z_elem(4)  = e_n 
    self% ind_Z_elem(5)  = e_o 
    self% ind_Z_elem(6)  = e_ne
    self% ind_Z_elem(7)  = e_na
    self% ind_Z_elem(8)  = e_mg
    self% ind_Z_elem(9)  = e_al
    self% ind_Z_elem(10) = e_si
    self% ind_Z_elem(11) = e_s
    self% ind_Z_elem(12) = e_ar
    self% ind_Z_elem(13) = e_ca
    self% ind_Z_elem(14) = e_cr
    self% ind_Z_elem(15) = e_mn
    self% ind_Z_elem(16) = e_fe
    self% ind_Z_elem(17) = e_ni

  end subroutine set_op_mono_elem_indices

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine set_op_mono_chem_id(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: num_elem

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: set_op_mono_chem_id: get_table_ptr failed for id=', id
       return
    endif

    num_elem = self% num_elem
    if (allocated(self% chem_id)) deallocate(self% chem_id)
    allocate(self% chem_id(num_elem), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: set_op_mono_chem_id: allocate failed'
       return
    endif

    self% chem_id(1)  = get_nuclide_index('h1') ! ih1
    self% chem_id(2)  = get_nuclide_index('he4') ! ihe4
    self% chem_id(3)  = get_nuclide_index('c12') ! ic12
    self% chem_id(4)  = get_nuclide_index('n14') ! in14
    self% chem_id(5)  = get_nuclide_index('o16') ! io16
    self% chem_id(6)  = get_nuclide_index('ne20') ! ine20
    self% chem_id(7)  = get_nuclide_index('na23') ! ina23
    self% chem_id(8)  = get_nuclide_index('mg24') ! img24
    self% chem_id(9)  = get_nuclide_index('al27') ! ial27
    self% chem_id(10) = get_nuclide_index('si28') ! isi28
    self% chem_id(11) = get_nuclide_index('s32') ! is32
    self% chem_id(12) = get_nuclide_index('ar40') ! iar40
    self% chem_id(13) = get_nuclide_index('ca40') ! ica40
    self% chem_id(14) = get_nuclide_index('cr52') ! icr52
    self% chem_id(15) = get_nuclide_index('mn55') ! imn55
    self% chem_id(16) = get_nuclide_index('fe56') ! ife56
    self% chem_id(17) = get_nuclide_index('ni58') ! ini58

  end subroutine set_op_mono_chem_id

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! Based on the choice of mixture, retrieve X, Y, Z and all metal abundances in mass fraction

  subroutine init_original_Z_fracs(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: mixture
    real(dp), dimension(num_chem_elements) :: Z_fracs
    real(dp) :: base_X, base_Y, base_Z

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_original_Z_fracs: get_table_ptr failed for id=', id
       return
    endif

    mixture = self% mixture

    if (mixture < AG89_zfracs) then
        write(*,*) 'Error: op_mono_lib: init_original_Z_fracs: self% mixture should be >= 1 (AG89_zfracs)'
        ierr = -1
        return
    endif
    if (mixture > A09_Prz_zfracs) then
        write(*,*) 'Error: op_mono_lib: init_original_Z_fracs: self% mixture should be <= 8 (A09_Prz_zfracs)'
        ierr = -1
        return
    endif

    ! retrieve all mass fractions of the selected mixture
    select case(mixture)
      case (AG89_zfracs)
           Z_fracs(1:num_chem_elements) = AG89_element_zfrac(1:num_chem_elements)
           base_Z = AG89_zsol
           base_Y = AG89_ysol
      case (GN93_zfracs)
           Z_fracs(1:num_chem_elements) = GN93_element_zfrac(1:num_chem_elements)
           base_Z = GN93_zsol
           base_Y = GN93_ysol
      case (GS98_zfracs)
           Z_fracs(1:num_chem_elements) = GS98_element_zfrac(1:num_chem_elements)
           base_Z = GS98_zsol
           base_Y = GS98_ysol
      case (L03_zfracs)
           Z_fracs(1:num_chem_elements) = L03_element_zfrac(1:num_chem_elements)
           base_Z = L03_zsol
           base_Y = L03_ysol
      case (AGS05_zfracs)
           Z_fracs(1:num_chem_elements) = AGS05_element_zfrac(1:num_chem_elements)
           base_Z = AGS05_zsol
           base_Y = AGS05_ysol
      case (AGSS09_zfracs)
           Z_fracs(1:num_chem_elements) = AGSS09_element_zfrac(1:num_chem_elements)
           base_Z = AGSS09_zsol
           base_Y = AGSS09_ysol
      case (L09_zfracs) 
           Z_fracs(1:num_chem_elements) = L09_element_zfrac(1:num_chem_elements)
           base_Z = L09_zsol
           base_Y = L09_ysol
      case (A09_Prz_zfracs)
           Z_fracs(1:num_chem_elements) = A09_Prz_zfrac(1:num_chem_elements)
           base_Z = A09_Prz_zsol
           base_Y = A09_Prz_ysol
      case default
           write(*,*) 'Error: op_mono_lib: init_original_Z_fracs: self% mixture not suppported.'
           ierr = -1
           return
    end select

    base_X = (1d0 - base_Y - base_Z)

    ! initial XYZ attributes
    self% base_X = base_X
    self% base_Y = base_Y
    self% base_Z = base_Z

    ! Z_fracs are normalized metal abundances w.r.t. the metallicity
    ! convert Z_fracs to mass fractions
    Z_fracs(:) = Z_fracs(:) * base_Z

    allocate(self% original_Z_fracs(num_chem_elements), stat=ierr)
    if (ierr /= 0) then
       write(*,*) 'Error: op_mono_lib: init_original_Z_fracs: allocate original_Z_fracs failed'
       return
    endif
    self% original_Z_fracs(:) = Z_fracs(:)

  end subroutine init_original_Z_fracs

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! Import the public element specifications for OP Mono from kap_def into self%

  subroutine set_op_mono_elements(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: num_elem

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: set_op_mono_elements: get_table_ptr failed for id=', id
       return
    endif

    num_elem = self% num_elem

    allocate(self% elem_Z(num_elem), &
             self% elem_name(num_elem), &
             self% elem_mass(num_elem), &
             stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: set_op_mono_elements: allocate failed'
       return
    endif
    self% elem_Z(1:num_elem)    = op_mono_element_Z(1:num_elem)
    self% elem_name(1:num_elem) = op_mono_element_name(1:num_elem)
    self% elem_mass(1:num_elem) = op_mono_element_mass(1:num_elem)

  end subroutine set_op_mono_elements

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! set the original mass fractions of metals as retrieved from chem_def, without modifying it.

  subroutine set_original_Zi(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self 
    integer :: i, num_elem, ind

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: set_original_Zi: get_table_ptr failed for id=', id
       return
    endif

    num_elem = self% num_elem

    allocate(self% base_mass_fracs(num_elem), self% mass_fracs(num_elem), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: set_original_Zi: allocate mass_fracs failed'
       return
    endif

    ! base_X and base_Y should be set manually
    self% base_mass_fracs(1)  = self% base_X
    self% base_mass_fracs(2)  = self% base_Y

    ! get only metal abundances in mass fractions
    do i = 3, num_elem
       ind                 = self% ind_Z_elem(i)
       self% base_mass_fracs(i) = self% original_Z_fracs(ind)
    enddo
    self% base_mass_fracs(3:) = self% base_mass_fracs(3:)/sum(self% base_mass_fracs(3:)) * self% base_Z
    self% mass_fracs(1:num_elem) = self% base_mass_fracs(1:num_elem)

  end subroutine

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine get_abar_zbar(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: num_elem, num_isos
    integer, dimension(:), allocatable  :: chem_id
    real(dp), dimension(:), allocatable :: Xi
    real(dp) :: X, Y, Z
    real(dp) :: abar, zbar, z2bar, ye, mass_correction, sumx
    real(dp), dimension(:), allocatable :: dabar_dx, dzbar_dx, dmc_dx
    logical, parameter :: skip_partials = .true.

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_abar_zbar: get_table_ptr failed'
       return
    endif
    num_elem = self% num_elem
    num_isos = self% num_elem

    allocate(chem_id(num_isos), Xi(num_isos), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_abar_zbar: allocate failed'
       return
    endif
    chem_id(1:num_isos) = self% chem_id(1:num_elem)
    Xi(1:num_isos)      = self% mass_fracs(1:num_elem)
    X                   = self% mass_fracs(1)
    Y                   = 1d0 - (self% X + self% Z) 

    call get_composition_info(num_isos, chem_id, Xi, X, Y, Z, & 
            abar, zbar, z2bar, ye, mass_correction, &
            sumx, skip_partials, dabar_dx, dzbar_dx, dmc_dx)

    self% abar  = abar
    self% zbar  = zbar

  end subroutine get_abar_zbar


  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! Print the values of few key variables of the derived type on stdout

  subroutine list_table_content(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: i 
    real(dp) :: Y

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: list_table_content: get_table_ptr failed for id=', id
       return
    endif
    Y = 1d0 - self% X - self% Z

    print *
    print *, ' op_mono_lib: list_table_content: '
    print *
    print "(a5, 2a10, 2a16)", 'Num.', 'element', 'chem_id', 'mass fraction', 'mass fraction'
    print "(a5, 2a10, 2a16)", '', 'name', '', 'original', 'in table'
    do i = 1, self% num_elem
       print "(i5, a10, i10, 2f16.8)", i, self% elem_name(i), self% chem_id(i), &
             self% base_mass_fracs(i), self% mass_fracs(i)
    enddo

    print "('  base_X=', F0.4, ', base_Y=', F0.4, ', base_Z=', F0.4)", &
               self% base_X, self% base_Y, self% base_Z
    print "('       X=', F0.4, ',      Y=', F0.4, ',      Z=', F0.4)", &
               self% X, Y, self% Z
    print *

  end subroutine list_table_content

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! Modify XYZ 

  subroutine modify_XYZ(id, ierr)
    integer, intent(in) :: id 
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: num_elem
    real(dp) :: X, Y, Z
    real(dp), parameter :: tolerance = 1d-12
    logical  :: sum_is_OK

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: modify_XYZ: get_table_ptr failed'
       return
    endif

    num_elem = self% num_elem
    X        = self% X 
    Z        = self% Z 
    Y        = 1d0 - (X + Z)

    ! Sometimes, the Y=0.0 case give a small negative Y; here, we capture and fix that
    if (Y < 0d0) then 
       X = X + Y 
       Y = 0d0
       self% base_X = X
    endif

    self% mass_fracs(1) = X 
    self% mass_fracs(2) = Y 
    self% mass_fracs(3:num_elem) = self% base_mass_fracs(3:num_elem) / self% base_Z * Z

    sum_is_OK = (abs(1d0 - sum(self% mass_fracs(1:num_elem))) <= tolerance)
    if (.not. sum_is_OK) then 
       write(*,*) 'Error: op_mono_lib: modify_XYZ: sum of mass fractions is not 1.0: ', &
                  abs(1d0 - sum(self% mass_fracs)), sum(self% mass_fracs)
       print*, size(self% mass_fracs)
       print*, self% mass_fracs
       print*, self% Z, sum(self% mass_fracs(3:))
       print*, self% base_Z, sum(self% base_mass_fracs(3:))
       ierr = -1
       return 
    endif

  end subroutine modify_XYZ

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! Multiply the abundance of only one metal, Z_i, with a fixed factor without "re-normalizing" 
  ! all metals to base_Z or to Z

  subroutine modify_Zi_at_fixed_Z(id, the_elem_name, factor, ierr)
    integer, intent(in) :: id
    character(len=2), intent(in) :: the_elem_name
    real(dp), intent(in) :: factor
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self 
    integer :: i, num_elem, ind_elem
    real(dp) :: base_Z

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: modify_Zi_at_fixed_Z: get_table_ptr failed for id=', id
       return
    endif

    num_elem = self% num_elem
    base_Z   = self% base_Z
    ind_elem = 0
    do i = 3, num_elem  ! loop only over metals, hence start from index 3
       if (the_elem_name == self% elem_name(i)) then 
          ind_elem = i
          exit 
       endif
    enddo
    if (ind_elem == 0) then 
       print "('Error: op_mono_lib: modify_Zi_at_fixed_Z: ', A2, ' is unavailable.')", the_elem_name
       ierr = -1
       return
    endif

    self% mass_fracs(ind_elem) = self% mass_fracs(ind_elem) * factor

  end subroutine modify_Zi_at_fixed_Z

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! Re-normalize the metal abundances Z_i to base_Z

  subroutine renormalize_Zi_to_base_Z(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: i, num_elem
    real(dp) :: base_Z
    real(dp) :: sum_Z, factor, tol 

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: renormalize_Zi_to_base_Z: get_table_ptr failed for id=', id
       return
    endif

    num_elem = self% num_elem
    base_Z   = self% base_Z
    sum_Z    = sum(self% mass_fracs(3 : num_elem))
    if ((base_Z > 1d-6) .and. (sum_Z < 1d-12)) return 

    factor   = base_Z / sum_Z
    self% mass_fracs(3 : num_elem) = self% mass_fracs(3 : num_elem) * factor

    tol      = abs(sum(self% mass_fracs(3:num_elem)) - base_Z)
    if (tol >= 1d-12) then 
       write(*,*) 'Error: op_mono_lib: renormalize_Zi_to_base_Z: large tolerance1, tol=', tol 
       ierr = -1
       return
    endif

    tol      = abs(1d0 - sum(self% mass_fracs(1:num_elem)))
    if (tol >= 1d-12) then 
       write(*,*) 'Error: op_mono_lib: renormalize_Zi_to_base_Z: large tolerance 2; tol=', tol 
       ierr = -1
       return
    endif

  end subroutine renormalize_Zi_to_base_Z

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine renormalize_Zi_metals_to_unity(id, normalized_metals, ierr)
    integer, intent(in) :: id
    real(dp), allocatable, dimension(:) :: normalized_metals  ! (17-2); excluding H and He
    integer, intent(out) :: ierr 

    type(op_mono_table), pointer :: self
    integer :: num_elem
    real(dp) :: sum_Z  

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: renormalize_Zi_metals_to_unity: get_table_ptr failed'
       return
    endif
    num_elem = self% num_elem
    allocate(normalized_metals(num_elem-2))

    sum_Z = sum(self% mass_fracs(3:num_elem))
    normalized_metals(1:num_elem-2) = self% mass_fracs(3:num_elem) / sum_Z

  end subroutine renormalize_Zi_metals_to_unity

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine init_kappa(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: mixture
    character(len=256) :: kappa_file_prefix, kappa_CO_prefix, &
                          kappa_lowT_prefix
    real(dp) :: kappa_blend_logT_upper_bdy, kappa_blend_logT_lower_bdy, &
                type2_logT_lower_bdy
    logical, parameter :: use_cache = .false.

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_kappa: get_table_ptr failed for id=', id
       return
    endif

    mixture = self% mixture

    select case(mixture)
      case (AGSS09_zfracs)
            kappa_file_prefix = 'OP_a09'
            kappa_CO_prefix   = 'a09_co'
      case (A09_Prz_zfracs)
            kappa_file_prefix = 'OP_a09_p13'
            kappa_CO_prefix   = 'a09_p13_co'
      case default
            write(*,*) 'Error: OP tables for the requested mixture not available yet'
            ierr = -1
            return 
    end select
    kappa_lowT_prefix = 'lowT_fa05_a09p'  ! e.g. 'lowT_fa05_a09p'
    kappa_blend_logT_upper_bdy = 3.75 ! 0 = use default
    kappa_blend_logT_lower_bdy = 3.75 ! 0 = use default
    type2_logT_lower_bdy = 0 ! use default

    call kap_init(trim(kappa_file_prefix), trim(kappa_CO_prefix), trim(kappa_lowT_prefix), &        
                  kappa_blend_logT_upper_bdy, kappa_blend_logT_lower_bdy, &
                  type2_logT_lower_bdy, use_cache, '', ierr) 

    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_kappa: kap_init failed'
       return
    endif
    if (.not. kap_is_initialized) then 
       write(*,*) 'Error: op_mono_lib: init_kappa: kap_is_initialized is still .false.'
       ierr = -1
       return
    else 
       write(*,*) 'init_kappa succeeded.'
    endif

    call load_op_mono_data(trim(self% op_mono_data_path), trim(self% op_mono_data_cache_filename), ierr)
    if (ierr /= 0) then 
       write(*,'(a,i4)') 'Error: op_mono_lib: init_kappa: load_op_mono_data failed; ierr=', ierr
       return
    else 
       write(*,*) 'OP Mono data successfully loaded.'
       print *
    endif 

  end subroutine init_kappa

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! call get_op_mono_args() only once, and pass the output arguments for later use

  subroutine prepare_kappa(id, ierr)
    integer, intent(in) :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    integer :: species, num_elem, nel
    integer :: nptot, ipe, nrad 
    integer, allocatable, dimension(:) :: chem_id
    real(dp), dimension(:), allocatable :: mass_fracs
    real(dp), parameter :: min_X_to_include = 1d-12
    integer, allocatable, dimension(:)  :: izzp
    real(dp), allocatable, dimension(:) :: fap, fac

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: prepare_kappa: get_table_ptr failed for id=', id
       return
    endif

    num_elem  = self% num_elem
    species   = num_elem

    ! get the kap_handle only once and for the use of all cases
    if (.not. kap_handle_is_available) then 
      kap_handle = alloc_kap_handle(ierr)
      if (ierr /= 0) then 
         write(*,*) 'Error: op_mono_lib: prepare_kappa: alloc_kap_handle failed'
         return
      endif
      kap_handle_is_available = .true. 
    endif

    ! allocate and store pre-defined OP Mono element specifications from kap_def
!    call do_kap_def_init(.false.)
    call set_op_mono_elements(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: init_mixture: set_op_mono_elements failed'
       return
    endif

    allocate(mass_fracs(num_elem), chem_id(num_elem), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: prepare_kappa: allocate failed'
       return
    endif
    mass_fracs(:) = self% mass_fracs(:)
    chem_id(:)    = self% chem_id(:)

    call set_kappa_factors(id, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: prepare_kappa: set_kappa_factors failed'
       return
    endif

    allocate(izzp(num_elem), fap(num_elem), fac(num_elem), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: get_kappa: allocate izzp, fap failed'
       return
    endif

    call get_op_mono_args(species, mass_fracs, min_X_to_include, chem_id, &
                          self% kappa_factor, nel, izzp, fap, fac, ierr)
    if (ierr /= 0) then 
        write(*,*) 'Error: op_mono_lib: get_kappa: get_op_mono_args failed'
        return
    endif 
!     if (nel /= species) then 
!        write(*,*) 'Error: op_mono_lib: prepare_kappa: number of species should match nel!', species, nel 
!        write(*,*) '(X, Z) = ', self% X, self% Z
!        ierr = -1
!        return
!     endif
    
    allocate(self% izzp(num_elem), self% fap(num_elem), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: prepare_kappa: allocate self% izzp, self% fap failed'
       return
    endif
    self% izzp(:) = izzp(:)
    self% fap(:)  = fap(:)

    call get_op_mono_params(nptot, ipe, nrad)
    self% nptot = nptot
    self% ipe   = ipe
    self% nrad  = nrad
    self% nel   = nel

    deallocate(izzp, fap)

  end subroutine prepare_kappa

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine set_kappa_factors(id, ierr)
    integer, intent(in) :: id 
    integer, intent(out) :: ierr 

    type(op_mono_table), pointer :: self 

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: set_kappa_factors: get_table_ptr failed'
       return 
    endif

    allocate (self% kappa_factor(self% num_elem), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: set_kappa_factors: allocate failed'
       return 
    endif
    self% kappa_factor(1 : self% num_elem) = 1d0
    self% kappa_factor(16) = self% Fe_kappa_factor
    self% kappa_factor(17) = self% Ni_kappa_factor

  end subroutine set_kappa_factors

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! The limits of allowed logT array are hardcoded in kap/private/op_ev.f, lines 91 and 94
  ! The envelope (3.75<=logT<=6) is twice densly covered, while the interior (logT>=6.05) has a 
  ! more coarse coverage

  function get_logT_array() result(logT_array)
    real(dp), parameter :: logT_env_from = 3.750d0, &   ! envelope
                           logT_env_to   = 6d0, &
                           logT_env_step = 0.025d0, &
                           logT_int_from = 6.050d0, &   ! interior
                           logT_int_to   = 7.9d0, &
                           logT_int_step = 0.050d0
    integer, parameter :: num_logT_env = int((logT_env_to - logT_env_from) / logT_env_step + 1), &
                          num_logT_int = int((logT_int_to - logT_int_from) / logT_int_step + 1), &
                          num_logT     = num_logT_env + num_logT_int + 1 ! the last logT value has step 0.10
    real(dp), dimension(num_logT) :: logT_array

    integer :: i 

    do i = 1, num_logT_env + 1
       logT_array(i) = logT_env_from + (i-1) * logT_env_step
    enddo
    do i = num_logT_env+1, num_logT-1
       logT_array(i) = logT_int_from + (i-num_logT_env-1) * logT_int_step
    enddo
    logT_array(num_logT) = 8d0

  end function get_logT_array

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  function get_logR_array() result(logR_array)
    real(dp), parameter :: logR_from = -8d0, &
                           logR_to = 1d0, &
                           logR_step = 0.250d0
    integer, parameter :: num_logR = int((logR_to - logR_from) / logR_step + 1)
    real(dp), dimension(num_logR) :: logR_array

    integer :: i 

    do i = 1, num_logR
       logR_array(i) = logR_from + (i-1) * logR_step
    enddo

  end function get_logR_array

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  function get_logRho(logT, logR) result(logRho)
    real(dp), intent(in) :: logT, logR
    real(dp)  :: logRho

    logRho = logR + 3d0 * logT - 18d0

  end function get_logRho

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! The variables that are set by users through the inlist, are then read by read_inlist_op_mono,
  ! and stored in the user_settings derived type. Then, to transfer the data to the lib module,
  ! we update the identical variables between the two derived types defined in the lib module and 
  ! here

  subroutine update_lib_with_io(id, set, ierr)
    integer, intent(in) :: id
    type(user_settings) :: set 
    integer, intent(out) :: ierr
    type(op_mono_table), pointer :: self

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: op_mono_lib: update_lib_with_io: get_table_ptr failed for id=', id
       return
    endif

    self% mixture            = set% mixture

    self% Fe_kappa_factor    = set% Fe_kappa_factor
    self% Ni_kappa_factor    = set% Ni_kappa_factor

    self% comment_Type_I     = set% comment_Type_I
    self% dir_save_Type_I    = set% dir_save_Type_I
    self% prefix_Type_I      = set% prefix_Type_I
    self% suffix_Type_I      = set% suffix_Type_I
    self% table_name_Type_I  = set% table_name_Type_I

    self% comment_Type_II    = set% comment_Type_II
    self% dir_save_Type_II   = set% dir_save_Type_II
    self% prefix_Type_II     = set% prefix_Type_II
    self% suffix_Type_II     = set% suffix_Type_II
    self% table_name_Type_II = set% table_name_Type_II

    self% op_mono_data_path  = set% op_mono_data_path
    self% op_mono_data_cache_filename = set% op_mono_data_cache_filename

  end subroutine update_lib_with_io

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  function get_XZ_pairs_Type_I() result(XZ_pair)
    real(dp), dimension(2, num_id_Type_I) :: XZ_pair

    include 'XZ_pairs_Type_I.inc'

  end function get_XZ_pairs_Type_I

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine set_table_XZ(id, X, Z, ierr) 
    integer, intent(in) :: id 
    real(dp), intent(in) :: X, Z
    integer, intent(out) :: ierr 

    type(op_mono_table), pointer :: self
!    real(dp), parameter :: tolerance = epsilon(real(0.0, kind=dp))

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: set_table_XZ: get_table_ptr failed'
       return
    endif
    if (self% X >= 0 .or. self% Z >= 0) then 
       write(*,*) 'Error: op_mono_lib: set_table_XZ: X or Z already modified!; ', self% X, self% Z 
       ierr = -1
       return
    endif
    if ((X < 0d0) .or. (X > 1d0)) then 
       write(*,*) 'Error: op_mono_lib: set_table_XZ: X out of range: ', X 
       ierr = -1 
       return
    endif
    if ((Z < 0d0) .or. (Z > 1d0)) then 
       write(*,*) 'Error: op_mono_lib: set_table_XZ: Z out of range: ', Z 
       ierr = -1 
       return
    endif

    self% X = X 
    self% Z = Z

  end subroutine set_table_XZ

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

end module op_mono_lib


