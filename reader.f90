!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!     Copyright (C) 2015   Ehsan Moravveji
!
!     This program is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.

!     This program is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.

!     You should have received a copy of the GNU General Public License
!     along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! - Date: Friday 22 May, 2015
! - Author: Ehsan Moravveji, KU Leuven, Belgium
! - Version: Adapted to MESA v.7385
! - Purpose: Tabulate OP Mono opacities
! - Name conventions:
!
! - ToDo list:

module reader

  use const_def
  use io_tools

  implicit none

  public

  type, public :: user_settings
    integer            :: mixture = 0

    real(dp)           :: Fe_kappa_factor
    real(dp)           :: Ni_kappa_factor
    character(len=256) :: comment_Type_I, dir_save_Type_I, prefix_Type_I, suffix_Type_I, &
                          table_name_Type_I, &
                          comment_Type_II, dir_save_Type_II, prefix_Type_II, suffix_Type_II, &
                          table_name_Type_II, &
                          op_mono_data_path, op_mono_data_cache_filename
  end type user_settings


  contains

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine read_inlist_op_mono(inlist_path, set, ierr)
    character(len=*), intent(in) :: inlist_path
    type (user_settings), intent(out) :: set
    integer, intent(out) :: ierr

    namelist / op_mono_nmlst / &
             mixture, &
             Fe_kappa_factor, Ni_kappa_factor, &
             comment_Type_I, dir_save_Type_I, prefix_Type_I, suffix_Type_I, &
             table_name_Type_I, &
             comment_Type_II, dir_save_Type_II, prefix_Type_II, suffix_Type_II, &
             table_name_Type_II, &
             op_mono_data_path, op_mono_data_cache_filename

    integer :: mixture
    real(dp) :: Fe_kappa_factor, Ni_kappa_factor
    character(len=256) :: comment_Type_I, dir_save_Type_I, prefix_Type_I, suffix_Type_I, &
                          table_name_Type_I, &
                          comment_Type_II, dir_save_Type_II, prefix_Type_II, suffix_Type_II, &
                          table_name_Type_II, &
                          op_mono_data_path, op_mono_data_cache_filename

    integer :: iounit

    ierr = 0
    call inquire_file_exists(trim(inlist_path), ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: reader: read_inlist_op_mono: inquire_file_exists failed, ierr=', ierr
       write(*,*) '       missing file: ', trim(inlist_path)
       return
    endif

    iounit = get_iounit()
    if (iounit == -1) then 
       write(*,*) 'Error: reader: read_inlist_op_mono: get_iounit failed'
       ierr = iounit
       return
    endif

    open(unit=iounit, file=trim(inlist_path), status='old', action='read', iostat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: reader: read_inlist_op_mono: open stetement failed'
       return
    endif

    read(unit=iounit, nml=op_mono_nmlst, iostat=ierr)

    set% mixture           = mixture

    set% Fe_kappa_factor   = Fe_kappa_factor
    set% Ni_kappa_factor   = Ni_kappa_factor

    set% comment_Type_I    = comment_Type_I
    set% dir_save_Type_I   = dir_save_Type_I
    set% prefix_Type_I     = prefix_Type_I
    set% suffix_Type_I     = suffix_Type_I
    set% table_name_Type_I = table_name_Type_I

    set% comment_Type_II   = comment_Type_II
    set% dir_save_Type_II  = dir_save_Type_II
    set% prefix_Type_II    = prefix_Type_II
    set% suffix_Type_II    = suffix_Type_II
    set% table_name_Type_II = table_name_Type_II

    set% op_mono_data_path = op_mono_data_path
    set% op_mono_data_cache_filename = op_mono_data_cache_filename

    close(unit=iounit)
    
    call free_iounit(iounit, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: reader: read_inlist_op_mono: free_iounit failed'
       return 
    endif

  end subroutine read_inlist_op_mono
  
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

end module reader
