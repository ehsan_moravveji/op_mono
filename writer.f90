!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!     Copyright (C) 2015   Ehsan Moravveji
!
!     This program is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.

!     This program is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.

!     You should have received a copy of the GNU General Public License
!     along with this program.  If not, see <http://www.gnu.org/licenses/>.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module writer

  use chem_def 

  use io_tools
  use op_mono_lib

  public

  contains

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine write_op_mono_type_I_tables(id_array, ierr)
    integer, intent(in), dimension(:) :: id_array
    integer, intent(out) :: ierr 

    integer :: id, num_tbls
    type(op_mono_table), pointer :: self

    ierr = 0
    num_tbls = size(id_array, dim=1)

    do id = 1, num_tbls
       call get_table_ptr(id, self, ierr)
       if (ierr /= 0) then 
          write(*,'(a,i4)') 'Error: writer: write_op_mono_type_I_tables: get_table_ptr failed for id: ', id 
          return
       endif

       if (.not. self% kap_is_computed) then 
          write(*,*) 'Error: writer: write_op_mono_type_I_tables: opacities not computed for table id = ', id 
          ierr = -1
          return
       endif

       call write_op_mono_type_I_table(id, ierr)
       if (ierr /= 0) then 
          write(*,'(a,i4)') 'Error: writer: write_op_mono_type_I_tables: write_op_mono_type_I_table failed for id: ', id 
          return
       endif
    enddo

  end subroutine write_op_mono_type_I_tables

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! To write the opacities in the OPAL Type I format

  subroutine write_op_mono_type_I_table(id, ierr)
    integer, intent(in)  :: id
    integer, intent(out) :: ierr

    type(op_mono_table), pointer  :: self
    integer :: iounit
    integer, parameter :: form = 1, version = 37
    integer :: i_T, n_T, n_R, k
    real(dp), allocatable :: log_kappa(:,:), logT_array(:), logR_array(:)
    character(len=256) :: table_filename
    character(len=256) :: dir_save_Type_I, table_name_Type_I
    character(len=256) :: comment_Type_I
    real(dp) :: X, Z, logR_min, logR_max, logT_min, logT_max
    character(len=256), parameter :: line_2 = '    form     version        X           Z' // &
                                     '          logRs    logR_min    logR_max' // &
                                     '       logTs    logT_min    logT_max'
    character(len=256), parameter :: line_5 = '   logT                       logR = logRho - 3*logT + 18'     
    character(len=256), parameter :: fmt_3 = '(i8, i12, 2f12.6, i12, 2f12.6, i12, 2f12.6)'
    character(len=256), parameter :: fmt_6 = '(8x, 37f8.3)'
    character(len=256), parameter :: fmt_8 = '(38f8.3)'

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: write_op_mono_type_I_table: get_table_ptr failed for id=', id
       return
    endif

    n_T  = size(self% logT_array, dim=1)
    n_R  = size(self% logR_array, dim=1)
    allocate(log_kappa(n_T, n_R), logR_array(n_R), logT_array(n_T), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: write_op_mono_type_I_table: allocate failed'
       return
    endif

    log_kappa(:,:)  = self% block_log_kappa(:,:)
    logT_array(:)   = self% logT_array(:)
    logR_array(:)   = self% logR_array(:)
    comment_Type_I  = self% comment_Type_I

    X              = self% X
    Z              = self% Z  
    logR_min       = minval(logR_array)
    logR_max       = maxval(logR_array)
    logT_min       = minval(logT_array)
    logT_max       = maxval(logT_array)

    iounit = get_iounit()
    if (iounit == -1) then
       write(*,*) 'Error: writer: write_op_mono_type_I_table: get_iounit failed'
       ierr = iounit
       return
    endif

    if (len_trim(self% table_name_Type_I) > 1) then 

      ! set the table_filename based on user's input
      dir_save_Type_I = trim(self% dir_save_Type_I)
      call check_dir_trailing_slash(dir_save_Type_I, ierr)
      if (ierr /= 0) then 
         write(*,*) 'Error: writer: write_op_mono_type_I_table: check_dir_trailing_slash failed'
         return
      endif
      table_filename = trim(dir_save_Type_I) // trim(self% table_name_Type_I)

    else  

      ! create a proper name for the table based on the settings
      call get_table_filename(id, .true., table_filename, ierr)
      if (ierr /= 0) then 
         write(*,*) 'Error: writer: write_op_mono_type_I_table: get_table_filename failed'
         return
      endif

    endif

    open(unit=iounit, file=trim(table_filename), iostat=ierr, status="Unknown", action="write")
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: write_op_mono_type_I_table: open failed for ', trim(table_filename)
       return
    endif

    ! Write the comment line
    write(unit=iounit, fmt='(a,2(a5,F4.2))', iostat=ierr, advance='Yes') trim(comment_Type_I), &
                                                              '; Fex', self% Fe_kappa_factor, &
                                                              '; Nix', self% Ni_kappa_factor
    if (ierr /= 0) return
    write(unit=iounit, fmt='(a)', iostat=ierr, advance='Yes') trim(line_2)
    if (ierr /= 0) return
    write(unit=iounit, fmt=fmt_3, iostat=ierr, advance='Yes') form, version, X, Z, n_R, &
                                                              logR_min, logR_max, n_T, logT_min, logT_max
    if (ierr /= 0) return
    write(unit=iounit, fmt='(a)', iostat=ierr, advance='Yes') '' ! just a blank line
    if (ierr /= 0) return
    write(unit=iounit, fmt='(a)', iostat=ierr, advance='Yes') trim(line_5)
    if (ierr /= 0) return
    write(unit=iounit, fmt=fmt_6, iostat=ierr, advance='Yes') logR_array
    if (ierr /= 0) return
    write(unit=iounit, fmt='(a)', iostat=ierr, advance='Yes') '' ! just a blank line

    do i_T = 1, n_T
       write(unit=iounit, fmt=fmt_8, iostat=ierr, advance='Yes') logT_array(i_T), (log_kappa(i_T, k), k=1, n_R)
       if (ierr /= 0) then 
          write(*,*) 'Error: writer: write_op_mono_type_I_table: write statement failed in the loop!'
          return
       endif
    enddo

    call free_iounit(iounit, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: write_op_mono_type_I_table: free_iounit failed'
       return
    endif

    print "(' writer: write_op_mono_type_I_table: saved: ', a)", trim(table_filename) !, len_trim(table_filename)

  end subroutine write_op_mono_type_I_table

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  ! Create a proper filename for tables based on their specifications
  ! for Type I tables, set Type_I to .ture., else it works for Type_II tables

  subroutine get_table_filename(id, Type_I, filename, ierr) 
    integer, intent(in) :: id 
    logical, intent(in) :: Type_I
    character(len=256), intent(out) :: filename
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    character(len=256) :: save_dir, prefix, suffix, mid_name
    integer :: k, k1

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: get_table_filename: get_table_ptr failed'
       return
    endif 

    if (Type_I) then 
       save_dir = self% dir_save_Type_I
       prefix   = self% prefix_Type_I
       suffix   = self% suffix_Type_I
    else 
       save_dir = self% dir_save_Type_II
       prefix   = self% prefix_Type_II
       suffix   = self% suffix_Type_II
    endif

    call check_dir(save_dir, .true., ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: get_table_filename: check_dir failed'
       return
    endif

    call check_dir_trailing_slash(save_dir, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: get_table_filename: check_dir_trailing_slash failed'
       return
    endif

    call get_table_mid_name(id, mid_name, ierr)

    filename = trim(save_dir) // trim(prefix) // trim(mid_name) // trim(suffix)
!     print*, trim(save_dir), trim(prefix), trim(mid_name), trim(suffix)

  end subroutine get_table_filename

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  subroutine get_table_mid_name(id, name, ierr)
    integer, intent(in) :: id
    character(len=*), intent(out) :: name 
    integer, intent(out) :: ierr

    type(op_mono_table), pointer :: self
    real(dp) :: X, Z, Fe_x, Ni_x
    real(sp) :: Z_fix
    integer :: mixture, int_z, int_m, int_x
    character(len=2)  :: str_z, str_m
    character(len=6)  :: str_x
    character(len=16) :: str_mixture
    character(len=6)  :: str_Fe, str_Ni
    character(len=13) :: str_Fe_Ni
    
    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: get_table_mid_name: get_table_ptr failed'
       return
    endif  

    mixture = self% mixture
    X       = self% X
    Z       = self% Z 
    Z_fix   = sngl(Z) * 1.0001
    Fe_x    = self% Fe_kappa_factor
    Ni_x    = self% Ni_kappa_factor

    select case(mixture)
      case (AG89_zfracs)
           str_mixture = 'AG89'
      case (GN93_zfracs)
           str_mixture = 'GN93'
      case (GS98_zfracs)
           str_mixture = 'GS98'
      case (L03_zfracs)
           str_mixture = 'L03'
      case (AGS05_zfracs)
           str_mixture = 'A05+Ne'
      case (AGSS09_zfracs)
           str_mixture = 'a09'
      case (L09_zfracs) 
           str_mixture = 'L09'
      case (A09_Prz_zfracs)
           str_mixture = 'a09_p13'
      case default
           write(*,*) 'Error: writer: get_table_mid_name: self% mixture not suppported. id=', id
           ierr = -1
           return
    end select

    write(str_Fe, '(a, f4.2)') 'Fe', Fe_x
    write(str_Ni, '(a, f4.2)') 'Ni', Ni_x
    str_Fe_Ni = trim(str_Fe) // '_' // trim(str_Ni)

    if (Z_fix < 1d-6) then 
       int_m = 0
       int_z = 0
    else if (Z_fix > 9d-5 .and. Z_fix < 1d-3) then 
       int_m = 4
       int_z = int(Z_fix * 10000.01d0)
    else if (Z_fix > 9d-4 .and. Z_fix < 1d-2 ) then 
       int_m = 3
       int_z = int(Z_fix * 1000.01d0)
    else if (Z_fix > 9d-3 .and. Z_fix < 1d-1) then 
       int_m = 2
       int_z = int(Z_fix * 100.01d0)
    else if (Z_fix > 9d-2 .and. Z_fix < 1) then 
       int_m = 1
       int_z = int(Z_fix * 10.01d0)
    else 
       print "('Error: writer: get_table_mid_name: Requested Z=', f10.8, ' is not allowed')", Z 
       ierr = -1
       return
    endif

    write(str_m, '(a1, i1)') 'm', int_m
    write(str_z, '(a1, i1)') 'z', int_z

    if (X < 1d-4) then 
      write(str_x, '(a3)') 'x00'
    else if (X >= 1d-4 .and. X < 0.991) then 
      write(str_x, '(a1, i2)') 'x', int(X * 100.01d0)
    else if (X > 0.991 .and. X < 0.9991d0) then 
      write(str_x, '(a1, f4.1)') 'x', X * 100d0
    else if (X > 0.9991 .and. X < 0.99991) then 
      write(str_x, '(a1, f5.2)') 'x', X * 100d0
    else if (X >= 0.999999d0) then 
      write(str_x, '(a4)') 'x100'
    else
      print "('Error: writer: get_table_mid_name: Requested X=', f10.8, ' is not allowed')", X 
      ierr = -1
      return
    endif

    name = trim(str_mixture) // '_' // trim(str_Fe_Ni) // '_' // str_z // str_m // '_' // trim(str_x)

  end subroutine get_table_mid_name

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

  subroutine write_mesa_uniform_xa_file(id, filename, ierr)
    integer, intent(in) :: id
    character(len=*), intent(in) :: filename
    integer, intent(out) :: ierr 

    type(op_mono_table), pointer :: self 
    integer :: iounit, i, num_elem
    real(dp), allocatable, dimension(:) :: bmf     ! bmf is just the acronym for base_mass_fracs()
    character(len=2), allocatable, dimension(:) :: names   ! (num_elem) = (17)
    integer, allocatable, dimension(:) :: Z_plus_N

    call get_table_ptr(id, self, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: write_mesa_uniform_xa_file: get_table_ptr failed'
       return
    endif
    num_elem = self% num_elem
    allocate(names(num_elem), bmf(num_elem), Z_plus_N(num_elem), stat=ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: write_mesa_uniform_xa_file: allocate failed'
       return
    endif
    names(:) = self% elem_name(:)
    bmf(:)   = self% base_mass_fracs(:)

    do i = 1, num_elem
       ind           = self% ind_Z_elem(i)
       print*, i, ind, chem_get_element_id(names(i))  ! from chem_lib
!        Z_plus_N(ind) = 
    enddo

    iounit = get_iounit()
    if (iounit == -1) then
       write(*,*) 'Error: writer: write_mesa_uniform_xa_file: get_iounit failed'
       ierr = iounit
       return
    endif

    open(unit=iounit, file=trim(filename), iostat=ierr, status="Unknown", action="write")
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: write_mesa_uniform_xa_file: open failed'
       return
    endif

    write(unit=iounit, fmt='(a4, 4x, f10.8)', iostat=ierr, advance='Yes') (names(k), bmf(k), k=1, num_elem)

    call free_iounit(iounit, ierr)
    if (ierr /= 0) then 
       write(*,*) 'Error: writer: write_mesa_uniform_xa_file: free_iounit failed'
       return
    endif

    print "(' writer: write_mesa_uniform_xa_file: saved: ', a)", trim(filename)

  end subroutine write_mesa_uniform_xa_file

  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  !%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

end module writer



